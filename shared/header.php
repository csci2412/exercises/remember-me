<?php include "init.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Remember me</title>
	<link href="assets/main.css" rel="stylesheet">
</head>
<body>
<nav>
	<?php if (isset($_SESSION['username'])): ?>
		<a href="logout.php">Logout</a>
	<?php else: ?>
		<a href="index.php">Home</a>
		<a href="register.php">Register</a>
	<?php endif; ?>
</nav>
<main>