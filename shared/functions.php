<?php

class DbHelper
{
	public $conn;

	public const USERNAME_ERROR = 1062;

	public function __construct(string $host, string $user, string $pass, string $db = null)
	{
		try {
			$this->conn = new mysqli($host, $user, $pass, $db);
		} catch (\Throwable $e) {
			print 'houston we have a problem...' . $e->getMessage();
		}
	}
}

function getConfig() : array {
	return parse_ini_file(__dir__ . '/../config.ini');
}

function getDbHelper(array $config) : DbHelper {
	$params = json_decode(file_get_contents($config['etc_directory'] . '/db-params.json'));

	return new DbHelper($params->host, $params->user, $params->pass, $params->schema);
}

function registerUser(DbHelper $db, string $user, string $pass, string $confirm) : void {
	if (empty($user) || empty($pass) || empty($confirm)) {
		displayMsg('All fields required!', true);
		
		return;
	}

	if ($pass !== $confirm) {
		displayMsg('passwords do not match!', true);
		
		return;
	}

	$hash = password_hash($pass, PASSWORD_DEFAULT);

	$q = "INSERT users (username, pass_hash) VALUES ('$user', '$hash')";
	$result = $db->conn->query($q);

	if (!$result) {
		if ($db->conn->errno === DbHelper::USERNAME_ERROR) {
			$msg = 'Username already taken';
		} else {
			'Problem registering user';
		}
		displayMsg($msg, true);
		
		return;
	}

	header('Location: index.php');
}

/**
 * this function parses the remember_me cookie by base64-decoding, then 
 * exploding the string based on the | character, because the value is 
 * in the form "id|token". Should return [id, token] as an array or
 * an empty array.
 */
function parseCookie() : array {
	$vals = explode('|', base64_decode($_COOKIE['remember_me']));

	if (count($vals) !== 2) {
		return [];
	}
	
	return [
		$vals[0] ?? 0,
		$vals[1] ?? '',
	];
}

/**
 * this function tries using the remember_me cookie to validate
 * user login
 */
function tryRemember(DbHelper $db) : void {
	// if we already have username in the session, or the cookie isn't set, return
	if (isset($_SESSION['username']) || !isset($_COOKIE['remember_me'])) {
		return;
	}
	
	// parse the cookie into local variables, use the id to get the token from db
	list($id, $token) = parseCookie();	
	$result = $db->conn->query('select token, user_id from tokens where id = ' . $id);

	// if no result, return
	if (!$result) return;

	$row = $result->fetch_object();

	// if the cookie token doesn't match db token, return
	if ($row->token !== $token) {
		return;
	}

	// get the user record from db based on token user_id
	$userResult = $db->conn->query('select * from users where id = ' . $row->user_id);

	// if no user record, return
	if (!$userResult) return;

	// put username in session
	$user = $userResult->fetch_object();
	$_SESSION['username'] = $user->username;
	
	// get a new remember_me token (this time update)
	getNewRememberToken($db, $id, true);

	// redirect to secret-page.php if needed
	if ($_SERVER['PHP_SELF'] !== '/remember-me/secret-page.php') {
		header('Location: secret-page.php');
	}
}

/**
 * this function creates a new token, saves to db (insert or update), and sets a cookie with encoded token val
 */
function getNewRememberToken(DbHelper $db, int $id, bool $update = false) : void {
	// create sha256 hash from random int
	$token = hash('sha256', rand());

	// save the token to the database
	// we could be creating a new token on login or updating a token from "tryRemember"
	if ($update) {
		$stmt = $db->conn->prepare('update tokens set token = ? where id = ?');
	} else {
		$stmt = $db->conn->prepare('insert tokens(token, user_id) values(?,?)');
	}
	$stmt->bind_param('si', $token, $id);

	// return if there was a problem executing the statement
	if (!$stmt->execute()) {
		display('Problem remembering you', true);

		return;
	}

	// if not an update, get the new token record id
	if (!$update) {
		$id = $stmt->insert_id;
	}

	// set a base64-encoded cookie called "remember_me" in the format "id|token"
	// set an expiration time of 60 * 60 * 24 * 90
	setcookie('remember_me', base64_encode($id . '|' . $token), time() + 60 * 60 * 24 * 90);
}

function login(DbHelper $db, string $user, string $pass, bool $remember) : void {
	if (empty($user) || empty($pass)) {
		displayMsg('All fields required!', true);

		return;
	}

	$result = $db->conn->query("SELECT * FROM users WHERE username = '$user'");
	
	if (!$result) {
		displayMsg('Problem fetching user', true);

		return;
	}

	$user = $result->fetch_object();

	if (!password_verify($pass, $user->pass_hash)) {
		displayMsg('Bad password!', true);

		return;
	}

	// if remember checkbox is checked, get a new token, set cookie
	if ($remember) {
		getNewRememberToken($db, $user->id);
	}

	$_SESSION['username'] = $user->username;

	header('Location: secret-page.php');
}

function displayMsg(string $msg, bool $isError = false) : void {
	$class = 'msg';
	
	if ($isError) $class .= ' msg--error';
	
	echo "<p class=\"$class\">$msg</p>";
}